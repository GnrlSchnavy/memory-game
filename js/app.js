// List of set variables:
var modal = document.getElementById("modal");
var starScore = 5;
var isLoading, correctGuess = false;
var cardsArray, openCards = [];
var turnCounter, moves, sevond, matches = 0;
var winCounter = 0;
var firstTurn = true;
var lastCard = {name:"",pos:0};
var timer = null;
var cards = [{name:"fa-diamond",pos:-1},
                {name:"fa-paper-plane-o",pos:-1},
                {name:"fa-anchor",pos:-1},
                {name:"fa-bolt",pos:-1},
                {name:"fa-cube",pos:-1},
                {name:"fa-leaf", pos:-1},
                {name:"fa-bicycle",pos:-1},
                {name:"fa-bomb",pos:-1}
            ]
// Creates list of cards:
function createCardArray(){
    cardsArray = []
    for(var i = 0; i < cards.length; i++){
        cardsArray.push({name:cards[i].name,pos:i});
        cardsArray.push({name:cards[i].name,pos:i});
    }
    cardsArray = shuffle(cardsArray)
    for (var i = 0; i < cardsArray.length; i++){
        cardsArray[i].pos = i;
    }
    return cardsArray
}

function printArray(array){
    for (var i = 0; i < array.length; i++){
    }
}

function resetCards(){
    for (var i = 0; i < cardsArray.length; i++){
        var tile = document.getElementById(String(i))
        tile.className = "card"
        tile.childNodes[1].className = "fa " + cardsArray[i].name
    }
}

// Shuffle function from http://stackoverflow.com/a/2450976:
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function getCardAt(pos){
    return cardsArray[pos]
}

// Allows the cards to be showed and disabled:
function turnUp(pos){
    var tile = document.getElementById(String(pos))
    var newCard = String(cardsArray[pos].name)
    if(!(tile.classList.contains("open") || tile.classList.contains("show"))){
        tile.className += " open show"
        tile.childNodes[0].className = newCard
    }
}

function turnDown(card){
    card = document.getElementById(String(card.pos))
    card.className = "card"
}

function removeFromMemory(){
    console.log(openCards + "bef" + openCards.length)
    for(var i = 0; i <= openCards.length; i++){
        openCards.pop()
        console.log(openCards)
    }
}

function turnDownCardsInMemory(){
    for(var i = 0; i < openCards.length; i++){
        turnDown(openCards[i]);
    }
    isLoading = false;
}

function isTurnedUp(card){
    card = document.getElementById(String(card.pos))
    if(card.classList.contains("open") || card.classList.contains("show")){
        return true
    }
    else {
        return false
    }
}

// Changes cards color when there is a match:
function setColor(currentCard,lastCard){
    currentCard = document.getElementById(currentCard.pos)
    lastCard = document.getElementById(lastCard.pos)
    currentCard.className += "card match"
    lastCard.className += "card match"
}

function turnTile(pos){
    if(!isLoading){
            var card = getCardAt(pos)
            if(correctGuess){
                removeFromMemory()
                correctGuess = false
            }

            // Click on closed card:
            if(!isTurnedUp(card)){
                //Turn 1/2:
                if(firstTurn){
                    turnDownCardsInMemory()
                    removeFromMemory()
                    turnUp(card.pos)
                    openCards.push(card)
                    lastCard = card
                    correctGuess = false
                }
                //Turn 2/2:
                else{
                    turnUp(card.pos)
                    openCards.push(card)
                    if(card.name == lastCard.name && card.pos != lastCard.pos){
                        correctGuess=true
                        removeFromMemory()
                        setColor(card,lastCard)
                        winCounter++;
                        //remove from opencards
                        //leave open
                    }
                    else {
                    isLoading = true;
                    setTimeout(turnDownCardsInMemory, 700);
                    }
                    turnCounter++
                    checkRating();
                }
                
                firstTurn = !firstTurn
            }
            //click on open card
            setTurnCounter()
            }
        if(winCounter == 8){
            clearInterval(timer);
            winnerModal();
         }
}

function checkRating(){
    switch(true){
        case turnCounter == 28:
        document.getElementById("star2").className="";
        starScore -=1;
        break;
        case turnCounter == 24:
        document.getElementById("star3").className="";
        starScore -=1;
        break;
        case turnCounter == 20:
        document.getElementById("star4").className="";
        starScore -=1;
        break;
        case turnCounter == 16:
        document.getElementById("star5").className="";
        starScore -=1;
        break;
    }
    console.log(starScore)
}

function winnerModal(){
    var finaleTime = document.getElementById("finalTime");
    var finalMoves = document.getElementById("finalMoves");
    var finalRating = document.getElementById("finalRating");
    finaleTime.innerHTML = (document.getElementById("timer").innerHTML)
    finalMoves.innerHTML = turnCounter;
    var ul = document.getElementById("finalStarsRating");
    ul.innerHTML="";
   
    for (var i =0; i<starScore; i++){
        console.log(i)
        var li = document.createElement("li");
        li.setAttribute("class", "fa fa-star");
        ul.appendChild(li);
    }
    modal.style.display = "block";
}


//start
function startTime() {
var time = 0,
    elapsed = '0.0';
timer = setInterval(function()
{
    time += 100;

    elapsed = Math.floor(time / 100) / 10;
    if(Math.round(elapsed) == elapsed) { elapsed += '.0'; }

    document.getElementById("timer").innerHTML = elapsed;

}, 100);
}

function setTurnCounter(){
    document.getElementById("turns").innerHTML = turnCounter
}

// Restarts the game:
function restart(){
    winCounter=0;
    starScore = 5;
    document.getElementById("star5").className="fa fa-star";
    document.getElementById("star4").className="fa fa-star";
    document.getElementById("star3").className="fa fa-star";
    document.getElementById("star2").className="fa fa-star";
    modal.style.display="none";
    winCounter=7;
    createCardArray();
    resetCards();
    firstTurn = true;
    turnCounter = 0;
    setTurnCounter();
    clearInterval(timer);
    startTime();
};
